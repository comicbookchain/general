<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ComicsChain</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/creative.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">ComicsChain</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">

          @if ($allPrevios->count() > 0)
            @foreach($allPrevios->get() as $slide)
              <a reltype='allPrevios' href="/slideshow/{{$slideItem->comics_id}}/{{$slide->id}}">{{$slideItem->name}}
              </a>
            @endforeach
          @endif

          @if ($allNextOuter->count() > 0)
            @foreach($allNextOuter->get() as $slide)
              <a reltype='allNextOuter' href="/slideshow/{{$slideItem->comics_id}}/{{$slide->id}}">{{$slideItem->name}}
              </a>
            @endforeach
          @endif


          <a href="/game/{{$slideItem->comics_id}}/{{$slideItem->id}}/0">Add Before
              </a>

              <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="{{ $slideItem->image }}" alt="First slide">
                  </div>
                </div>
                @if (isset($slide))
                <a class="carousel-control-prev" href="/slideshow/{{$slideItem->comics_id}}/{{$slide->id}}" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                @endif

                <a class="carousel-control-next" onclick="window.location = document.getElementById('serbia0').getAttribute('myplaceforpeace')" href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

          <div class="col-lg-8 mx-auto text-center">
            <img src=""/>
          </div>

          <a href="/game/{{$slideItem->comics_id}}/0/{{$slideItem->id}}">Add After
              </a>


          @if ($allPreviosOuter->count() > 0)
            @foreach($allPreviosOuter->get() as $key => $slide)

              <input type="hidden" id="serbia{{$key}}" myplaceforpeace="/slideshow/{{$slideItem->comics_id}}/{{$slide->id}}" >

              <a reltype='allPreviosOuter'  href="/slideshow/{{$slideItem->comics_id}}/{{$slide->id}}">{{$slideItem->name}}
              </a>
            @endforeach
          @endif




          @if ($allNext->count() > 0)
            @foreach($allNext->get() as $slide)
              <a reltype='allNext' href="/slideshow/{{$slideItem->comics_id}}/{{$slide->id}}">{{$slideItem->name}}</a>
            @endforeach
          @endif
        </div>
      </div>
    </section>



    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/creative.min.js"></script>

  </body>

</html>
