<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ComicsChain</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        
          <a class="navbar-brand js-scroll-trigger" font href="#page-top">
            <font color="red">
                      ComicsChain
            </font>
          </a>
        
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about"><font color=red>всього по троху....</font></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services"><font color=red>умные слова</font></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio"><font color=red>комиксы</font></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>          <font color="red">
                      ComicsChain
            </font></strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5"><font color="red">Comics created by Lot of People</font></p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="/game">Draw Comics</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">всього по троху....</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">Существует много определений комикса, однако все они, в целом, сводятся к тому, что комикс представляет собой серию изображений, в которой рассказывается какая-либо история. Согласно большинству исследователей, комикс — это единство повествования и визуального действия. Скотт МакКлауд, автор книги «Суть комикса» (англ. Understanding comics), предлагает краткое определение «последовательные изображения», и более полное — «смежные рисунки и другие изображения в смысловой последовательности»[1]. Жорж Садуль определяет комиксы, как «рассказы в картинках»[4].
              <br/><br/>

            В комиксах не обязательно присутствует текст, существуют и «немые» комиксы с интуитивно понятным сюжетом (например, «Арзак» Жана Жиро). Но чаще всего прямая речь в комиксе передаётся при помощи «словесного пузыря», который, как правило, изображается в виде облачка, исходящего из уст, или, в случае изображения мыслей, из головы персонажа. Слова автора обычно помещают над или под кадрами комикса.
                          <br/><br/>

            Комиксы могут быть любыми и по литературному жанру, и по стилю рисования. В виде комиксов адаптируются даже произведения классиков литературы. Но исторически сложилось, что самые распространённые жанры комикса — приключения и карикатура. Этот стереотип долго портил репутацию комиксов.
                          <br/><br/>

            Рисунок в комиксе имеет некоторую долю условности. Он упрощается для скорости рисования и удобства восприятия и идентификации читателя с персонажем[1].

            По объёму комиксы варьируются от коротких «полосок» из нескольких (обычно трёх) картинок до объёмных графических романов и сериалов из множества выпусков.
                          <br/><br/>
            Комикс тесно связан с кино, и особенно с мультипликацией. Как отмечает всё тот же МакКлауд, «Фильм на плёнке — это очень медленный комикс. Пространство для комикса значит то же, что время для фильма»[1]. В английском языке слово «cartoon» — «карикатура» может обозначать и комикс, и мультфильм[5]. Очень многие японские мультфильмы «аниме» являются экранизацией японских же комиксов «манга»
                          <br/><br/>
            В XX веке комикс стал одним из популярных жанров массовой культуры. К этому времени комиксы, в основном, утратили комичность, за которую получили название. Основным жанром комиксов стали приключения: боевики, детективы, ужасы, фантастика, истории о супергероях.
            <br/><br/>
            Золотой век комиксов (англ. Golden Age of Comic Books) — название периода в истории американских комиксов, который длился (по разным оценкам) с 1938 года по 1955 год . Первые серьёзные шаги в развитии искусства графических новелл были сделаны в начале XX века, в поисках новых путей графической и визуальной коммуникации и самовыражения. Сначала комиксы носили чисто юмористический характер. Во многом это объясняется этимологией английского слова, определившего их название. В корне эта ситуация изменилась в июне 1938 года, когда в США появился персонаж Супермен (англ. Superman). Началом золотого века принято считать первое появление Супермена в комиксе Action Comics #1, вышедшего в 1938 году и изданного DC Comics. Появление Супермена было очень популярным, и вскоре супергерои буквально заполонили страницы комиксов[6]. Другими персонажами, долгое время пользовавшимся популярностью, стали Пластик (англ. Plastic Man), издаваемый издательством Quality Comics, а также детектив Спирит под авторством карикатуриста Уилла Айснера, который первоначально выходил в качестве дополнения, объединённого с воскресным выпуском газеты[7]. Всего за этот период было создано более 400 супергероев. Большинство из них сильно напоминали Супермена и не дожили до наших дней, однако именно тогда родились такие герои, как Бэтмен и Капитан Америка. Вторая мировая война оказала серьёзное влияние на содержание комиксов о супергероях — теперь герои сражались со странами Оси, а на обложках изображались супергерои, борющиеся с лидером нацистского движения. После победы над фашизмом стали появляться супергерои с ядерными способностями, например Атомный Громовержец и Атомный Человек[8][9][10]. Историки того времени считают, что детские персонажи помогли ослабить страх молодых читателей относительно перспектив атомной войны[11]. Кроме того, герои начали бороться с коммунистами, а некоторые были вовлечены в Корейскую войну. Тем не менее, после окончания Второй мировой войны популярность супергероев пошла на спад. В целом, именно во времена золотого века проявилось новое и по сей день главное направление в комиксах — супергерои и новые миры.


            </p>
            <a class="btn btn-light btn-xl js-scroll-trigger" href="/game">Рисовать комиксы!</a>
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">умные слова</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-gem text-primary mb-3 sr-icon-1"></i>
              <h3 class="mb-3">Alexander Z</h3>
              <p class="text-muted mb-0">Ales застолби на всех еду, пожалуйста. Я скоро буду, и тоже голодный. :)</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-paper-plane text-primary mb-3 sr-icon-2"></i>
              <h3 class="mb-3">Yura Yeustratchik</h3>
              <p class="text-muted mb-0">Встречаются два друга:<br/>
                          — Как дела?<br/>
                          — Да вот, основал новый стартап, очень быстро растём, развиваемся.<br/>
                          — Круто! А в чём суть стартапа?<br/>
                          — Я провёл исследование рынка и решил публичный дом открыть.<br/>
                          — Ого, интересно, и какие цены?<br/>
                          — Анал — $100, орал — $50.<br/>
                          — А классический секс?<br/>
                          — Понимаешь, это ж стартап. В общем, я пока один работаю.<br/>
</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-code text-primary mb-3 sr-icon-3"></i>
              <h3 class="mb-3">Евген Бойка</h3>
              <p class="text-muted mb-0">Сябры зычу посьпехаў у працы!
Вы крутыя!
Пабачымся на пітчы заўтра.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fas fa-4x fa-heart text-primary mb-3 sr-icon-4"></i>
              <h3 class="mb-3">ImaGuru</h3>
              <p class="text-muted mb-0">Head Full of Fears , Has no Space of Dreams!</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="p-0" id="portfolio">

      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">комиксы</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>

      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          @foreach(\App\Comics::all() as $key => $comics)
            <a href="/comics/{{  $comics->id }}">
          @endforeach
        </ol>


        <div class="carousel-inner">
          @foreach(\App\Comics::all() as $key => $comics)
            <div class="carousel-item
            @if ($key == 0)
              active
            @endif

            ">
              <img class="d-block w-100"  style="width:300px; height: auto;" src="{{ $comics->title_image }}" alt="{{ $comics->name }}">
            </div>
          @endforeach
        </div>



        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </section>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>
