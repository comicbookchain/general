<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/game', function () {
    return view('game');
});

Route::get('/game/{id}', function ($id) {
    return view('game' , array('id' => $id));
});

Route::get('/game/{id}/{before}/{after}', function ($id , $before , $after) {
    return view('game' , array('id' => $id , 'before' => $before , 'after' => $after));
});

Route::get('/trigger', 'HomeController@myartsave');

Route::post('/myartsave', 'HomeController@myartsave');
Route::post('/myartsave/{id}', 'HomeController@myartsave');

//
Route::get('/slideshow/{id}/{slide_id}' , 'HomeController@slideshow');

Route::get('/comics/{id}' , 'HomeController@comics');


Route::get('/test', function () {
	$url = 'http://dotnet:3128/explode/api/program/method';
	$data = '{"address": "1f77fe5e39f62a2aac934eb989e9fb8e363134ae28d805820015708711126732", "method": "HelloWorld", "args": []}';

	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/json\r\n",
	        'method'  => 'POST',
	        'content' => $data
	    )
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	if ($result === FALSE) { /* Handle error */ }

	var_dump($result);
});


