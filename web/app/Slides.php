<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slides extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slide';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
}
