<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comics;
use App\Slides;

use Config;
use ErrorException;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function myartsave(Request $request , $comicsId = false)
    {
        if (!$comicsId){
            $comics = new Comics();
            $comics->name = 'oooochenj klovij comics';
            $comics->title_image = $request->get('photo');
            $comics->save();

            $comicsId = $comics->id;
        }

        $slide = new Slides();
        $slide->name = 'cool slide';
        $slide->image = $request->get('photo');
        $slide->comics_id = $comicsId;

        if ($request->has('before')){
            $slide->next_id = $request->get('before');
            $slide->previous = $request->get('after');           
        }

        $failed = 0;

        try {

            $previousId = false;
            $nextId = false;

            if ($request->has('before')){
                $previousId = $slide->next_id = $request->get('before');
                $nextId = $slide->previous = $request->get('after');           
            }


            $url = 'http://dotnet:3128/explode/api/program/method';

            /**
                "address": ProgramAddress,
                "method": "NewSlide",
                "args": ["bytes."+SlideHash, "int32."+PreviousSlideId, "int32."+NextSlideId]
            **/

            $slideDetails = array(
                "address" => Config::get('app.explode_address'),
                "method" => "NewSlide",
                "args"  => [
                    "bytes.".md5($slide->image), 
                    "int32.".$previousId, 
                    "int32.".$nextId,
                ]    
            );


            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/json\r\n",
                    'method'  => 'POST',
                    'content' => json_encode($slideDetails)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) {
                $failed = 1;
            }
            
        } catch (ErrorException $e) {
            $failed = 1;
        }

        $slide->save();

        return json_encode(array(
            'id' => $comicsId, 
            'hash' => md5($slide->image) , 
            'failed' => $failed,
            'slide' => $slide->id
        ));
    }

    public function slideshow($comicsId , $slideShow){
        $slidesDetail = Slides::where('comics_id' , $comicsId)->where('id' , $slideShow);

        if ($slidesDetail->count() > 0){
            $slideItem = $slidesDetail->first();

            $allPrevios = Slides::where('comics_id' , $comicsId)->where('id' , $slideItem->next_id);

            $allNext = Slides::where('comics_id' , $comicsId)->where('id' , $slideItem->previous);


            $allPreviosOuter = Slides::where('comics_id' , $comicsId)->where('next_id' , $slideItem->id);

            $allNextOuter = Slides::where('comics_id' , $comicsId)->where('previous' , $slideItem->id);



            return view('slide' , array(
                'slideItem' => $slideItem , 
                'allPrevios' => $allPrevios , 
                'allPreviosOuter' => $allPreviosOuter,
                'allNextOuter' => $allNextOuter ,
                'allNext' => $allNext
            ));
        }

        return back();
    }


    public function comics($comixId)
    {
        $allPreviosOuter = Slides::where('comics_id' , $comixId)->orderBy('id' , 'ASC'); 

        if ($allPreviosOuter->count() > 0){
            return redirect('/slideshow/'.$comixId.'/'.$allPreviosOuter->first()->id);
        }else{
            return back();
        }
    }

}
