<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comics extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comics';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    
}
