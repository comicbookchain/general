<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Comics;
use App\Slides;

use Config;

class ComicsCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'explode:comics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Comics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $allComixes = Comics::whereNull('external_id');

        foreach ($allComixes->get() as $key => $comics) {
            $url = 'http://dotnet:3128/explode/api/program/method';

            /**
                "address": ProgramAddress,
                "method": "NewSlide",
                "args": ["bytes."+SlideHash, "int32."+PreviousSlideId, "int32."+NextSlideId]
            **/

            $slideDetails = array(
                "address" => Config::get('app.explode_address'),
                "method" => "NewSlide",
                "args"  => [
                    "bytes.".md5($comics->id), 
                    "int32.".rand(0,999), 
                    "int32.".rand(0,999),
                ]    
            );


            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/json\r\n",
                    'method'  => 'POST',
                    'content' => json_encode($slideDetails)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) { /* Handle error */ }
        }

    }
}
