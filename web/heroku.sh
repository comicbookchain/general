#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
    heroku config:set  $line
done < "./.env"

heroku config:set APP_ENV=heroku