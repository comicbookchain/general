namespace Expload {

	using Pravda;
	using System;

	[Program]
	public class contract {
		public int LastSlideId = 0;
		public int LastComicBookId = 0;
		public Mapping<int, Bytes> SlideToOwner = new Mapping<int, Bytes>();
		public Mapping<int, Bytes> SlideToHash = new Mapping<int, Bytes>();
		public Mapping<int, int> SlideToPreviousSlide = new Mapping<int, int>();
		public Mapping<int, int> SlideToNextSlide = new Mapping<int, int>();
		public Mapping<int, Bytes> ComicBookToOwner = new Mapping<int, Bytes>();
		public Mapping<int, Bytes> ComicBookToSlides = new Mapping<int, Bytes>();
		public Mapping<Bytes, uint> Donations = new Mapping<Bytes, uint>();
		public uint DonationsToPlatform = 0;

		public static void Main() { }

		public void Init() {
			LastSlideId = 0;
			LastComicBookId = 0;
			DonationsToPlatform = 0;
		}

		public int NewSlide(Bytes SlideHash, int PreviousSlideId, int NextSlideId)
		{
			LastSlideId += 1;
			SlideToOwner.put(LastSlideId, Info.Sender());
			SlideToHash.put(LastSlideId, SlideHash);
			SlideToPreviousSlide.put(LastSlideId, PreviousSlideId);
			SlideToNextSlide.put(LastSlideId, NextSlideId);
			return LastSlideId;
		}

		public int NewComicBook(Bytes Slides)
		{
			LastComicBookId += 1;
			ComicBookToOwner.put(LastComicBookId, Info.Sender());
			ComicBookToSlides.put(LastComicBookId, Slides);
			return LastComicBookId;
		}

		public int Donate(int ComicBookId, int Donation)
		{
			if(Donation <= 0)
			{
				return 0;
			}

	        Bytes Slides = ComicBookToSlides.getDefault(ComicBookId, Bytes.EMPTY);
			if(Slides == Bytes.EMPTY)
			{
				return 0;
			}

			int SlidesCount = Convert.ToInt32(Slides[0]);
			uint DonationToComicBookOwner = (uint) (Donation / 20); // 5% to comic book owner
			uint DonationToSlide = (uint) (Donation * 9 / 10 / SlidesCount); // 90% of donation divided by number of slides
			uint DonationToPlatform = (uint) (Donation - DonationToComicBookOwner - DonationToSlide * SlidesCount); // rest 5%

			Bytes ComicBookOwner = ComicBookToOwner.getDefault(ComicBookId, Bytes.EMPTY);
			if(ComicBookOwner != Bytes.EMPTY)
			{
				Donations.put(ComicBookOwner, Donations.getDefault(ComicBookOwner, 0) + DonationToComicBookOwner); //TODO: send xcoin
			}
	        for (int i = 0; i < SlidesCount; i++)
	        {
	            int SlideId = Convert.ToInt32(Slides[i + 1]);
				Bytes SlideOwner = SlideToOwner.getDefault(SlideId, Bytes.EMPTY);
				if(SlideOwner != Bytes.EMPTY)
				{
					Donations.put(SlideOwner, Donations.getDefault(SlideOwner, 0) + DonationToSlide); //TODO: send xcoin
				}
	        }
			DonationsToPlatform += DonationToPlatform; //TODO: send xcoin

			return 1;
		}

		public int GetLastSlideId()
		{
			return LastSlideId;
		}

		public int GetLastComicBookId()
		{
			return LastComicBookId;
		}

		public Bytes GetSlideToOwner(int Index)
		{
			return SlideToOwner.getDefault(Index, Bytes.EMPTY);
		}

		public Bytes GetSlideToHash(int Index)
		{
			return SlideToHash.getDefault(Index, Bytes.EMPTY);
		}

		public int GetSlideToPreviousSlide(int Index)
		{
			return SlideToPreviousSlide.getDefault(Index, 0);
		}

		public int GetSlideToNextSlide(int Index)
		{
			return SlideToNextSlide.getDefault(Index, 0);
		}

		public Bytes GetComicBookToOwner(int Index)
		{
			return ComicBookToOwner.getDefault(Index, Bytes.EMPTY);
		}

		public Bytes GetComicBookToSlides(int Index)
		{
			return ComicBookToSlides.getDefault(Index, Bytes.EMPTY);
		}

		public uint GetDonations(Bytes Address)
		{
			return Donations.getDefault(Address, 0);
		}

		public uint GetDonationsToPlatform()
		{
			return DonationsToPlatform;
		}

	}
}
