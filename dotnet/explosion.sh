#!/bin/bash

while true
do

	/etc/init.d/tinyproxy restart

	if [! -d /dotnet/expload-desktop-0.13.3 ]; then

		cd /dotnet/

		wget https://download.expload.com/expload-desktop/expload-desktop-0.13.3.zip

		unzip expload-desktop-0.13.3.zip

		cd /

	fi

	/dotnet/expload-desktop-0.13.3/bin/expload-desktop

done